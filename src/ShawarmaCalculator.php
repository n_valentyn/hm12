<?php


namespace App;


class ShawarmaCalculator
{
    private array $shawarma;

    public function add(ShawarmaInterface $shawarma)
    {
        $this->shawarma[] = $shawarma;
    }

    public function ingredients()
    {
        $ingredients = [];
        foreach ($this->shawarma as $shawarma){
            $ingredients = array_merge($ingredients, $shawarma->getIngredients());
        }

        return array_unique($ingredients);
    }

    public function price()
    {
        $total = 0;
        foreach ($this->shawarma as $shawarma){
            $total += $shawarma->getCost();
        }

        return $total;
    }
}