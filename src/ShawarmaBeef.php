<?php


namespace App;


class ShawarmaBeef implements ShawarmaInterface
{
    private string $title = 'Шаурма говяжья';
    private float $cost = 75;
    private array $ingredients = [
        'чесночный соус',
        'говяжий окорок',
        'огурцы маринованные',
        'маринованный лук с барбарисом и зеленью',
        'салат коул слоу',
        'тандырный лаваш',
        'помидоры свежие',
        'хумус',
        'соус тахин'
    ];

    /**
     * @inheritDoc
     */
    public function getCost(): float
    {
        return $this->cost;
    }

    /**
     * @inheritDoc
     */
    public function getIngredients(): array
    {
        return $this->ingredients;
    }

    /**
     * @inheritDoc
     */
    public function getTitle(): string
    {
        return $this->title;
    }
}