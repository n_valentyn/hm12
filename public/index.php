<?php

require_once '../vendor/autoload.php';

$calculator = new \App\ShawarmaCalculator();
$calculator->add(new \App\ShawarmaLamb());
$calculator->add(new \App\ShawarmaOdeska());
$calculator->add(new \App\ShawarmaBeef());

var_dump($calculator->ingredients());
var_dump($calculator->price());
